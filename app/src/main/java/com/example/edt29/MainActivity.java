package com.example.edt29;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private ImageView imageView1;
    private ImageView imageView2;
    private ImageView imageView3;
    private ImageView imageView4;
    private ImageView imageView5;

    private TextView textView10;
    private TextView textView11;
    private TextView textView20;
    private TextView textView21;
    private TextView textView30;
    private TextView textView31;
    private TextView textView40;
    private TextView textView41;
    private TextView textView50;
    private TextView textView51;

    private Button button100;
    private Button button200;
    private Button button300;
    private Button button400;
    private Button button500;

    private boolean check1 = true;
    private boolean check2 = true;
    private boolean check3 = true;
    private boolean check4 = true;
    private boolean check5 = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();

        //Hook
        imageView1 = findViewById(R.id.imageView1);
        imageView2 = findViewById(R.id.imageView2);
        imageView3 = findViewById(R.id.imageView3);
        imageView4 = findViewById(R.id.imageView4);
        imageView5 = findViewById(R.id.imageView5);
        textView10 = findViewById(R.id.textView10);
        textView11 = findViewById(R.id.textView11);
        textView20 = findViewById(R.id.textView20);
        textView21 = findViewById(R.id.textView21);
        textView30 = findViewById(R.id.textView30);
        textView31 = findViewById(R.id.textView31);
        textView40 = findViewById(R.id.textView40);
        textView41 = findViewById(R.id.textView41);
        textView50 = findViewById(R.id.textView50);
        textView51 = findViewById(R.id.textView51);
        button100 = findViewById(R.id.button100);
        button200 = findViewById(R.id.button200);
        button300 = findViewById(R.id.button300);
        button400 = findViewById(R.id.button400);
        button500 = findViewById(R.id.button500);

        imageView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (check1) {
                    animation(imageView1, textView10, textView11, button100);
                    check1 = false;
                } else {
                    animationBack(imageView1, textView10, textView11, button100);
                    check1 = true;
                }
            }
        });

        imageView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (check2) {
                    animation(imageView2, textView20, textView21, button200);
                    check2 = false;
                } else {
                    animationBack(imageView2, textView20, textView21, button200);
                    check2 = true;
                }
            }
        });

        imageView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (check3) {
                    animation(imageView3, textView30, textView31, button300);
                    check3 = false;
                } else {
                    animationBack(imageView3, textView30, textView31, button300);
                    check3 = true;
                }
            }
        });

        imageView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (check4) {
                    animation(imageView4, textView40, textView41, button400);
                    check4 = false;
                } else {
                    animationBack(imageView4, textView40, textView41, button400);
                    check4 = true;
                }
            }
        });

        imageView5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (check5) {
                    animation(imageView5, textView50, textView51, button500);
                    check5 = false;
                } else {
                    animationBack(imageView5, textView50, textView51, button500);
                    check5 = true;
                }
            }
        });

        button100.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Reservar_Activity.class);
                startActivity(intent);
            }
        });

        button200.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Reservar_Activity.class);
                startActivity(intent);
            }
        });

        button300.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Reservar_Activity.class);
                startActivity(intent);
            }
        });

        button400.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Reservar_Activity.class);
                startActivity(intent);
            }
        });

        button500.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Reservar_Activity.class);
                startActivity(intent);
            }
        });
    }

    private void animation(ImageView imgView, TextView txtV1, TextView txtV2, Button button) {
        ObjectAnimator objectAnimator1 = null;
        ObjectAnimator objectAnimator2 = null;
        ObjectAnimator objectAnimator3;
        ObjectAnimator objectAnimator4;
        ObjectAnimator objectAnimator5;

        objectAnimator1 = objectAnimator1.ofFloat(imgView, "RotationY", 0f, 180f);       //gira imatge
        objectAnimator1.setDuration(1000);
        objectAnimator2 = objectAnimator2.ofFloat(imgView, "alpha", 1f, 0.2f);             //obscureix imatge a la meitat del gir (90º)
        objectAnimator2.setStartDelay(500);

        objectAnimator3 = ObjectAnimator.ofFloat(txtV1, "alpha", 0f, 1f);          //els textos i botó només tenen animació d'opacitat
        objectAnimator3.setStartDelay(600);
        objectAnimator3.setDuration(1);

        objectAnimator4 = ObjectAnimator.ofFloat(txtV2, "alpha", 0f, 1f);
        objectAnimator4.setStartDelay(600);
        objectAnimator4.setDuration(1);

        objectAnimator5 = ObjectAnimator.ofFloat(button, "alpha", 0f, 1f);
        objectAnimator5.setStartDelay(600);
        objectAnimator5.setDuration(1);

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(objectAnimator1, objectAnimator2, objectAnimator3, objectAnimator4, objectAnimator5);
        animatorSet.start();
        button.setClickable(true);
    }

    private void animationBack(ImageView imgView, TextView txtV1, TextView txtV2, Button button) {
        ObjectAnimator objectAnimator1 = null;
        ObjectAnimator objectAnimator2 = null;
        ObjectAnimator objectAnimator3;
        ObjectAnimator objectAnimator4;
        ObjectAnimator objectAnimator5;

        objectAnimator1 = objectAnimator1.ofFloat(imgView, "RotationY", 180f, 0f);
        objectAnimator1.setDuration(1000);
        objectAnimator2 = objectAnimator2.ofFloat(imgView, "alpha", 0.2f, 1f);
        objectAnimator2.setStartDelay(500);

        objectAnimator3 = ObjectAnimator.ofFloat(txtV1, "alpha", 1f, 0f);
        objectAnimator3.setStartDelay(400);
        objectAnimator3.setDuration(1);

        objectAnimator4 = ObjectAnimator.ofFloat(txtV2, "alpha", 1f, 0f);
        objectAnimator4.setStartDelay(400);
        objectAnimator4.setDuration(1);

        objectAnimator5 = ObjectAnimator.ofFloat(button, "alpha", 1f, 0f);
        objectAnimator5.setStartDelay(400);
        objectAnimator5.setDuration(1);

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(objectAnimator1, objectAnimator2, objectAnimator3, objectAnimator4, objectAnimator5);
        animatorSet.start();
        button.setClickable(false);
    }
}